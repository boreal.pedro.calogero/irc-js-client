import { AppRegistry } from 'react-native';

import { App } from 'components/src/App';

AppRegistry.registerComponent('ircjsclient', () => App)
AppRegistry.runApplication('ircjsclient', {
  rootTag: document.getElementById('root'),
});
